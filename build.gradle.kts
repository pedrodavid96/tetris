/*
 * This file was generated by the Gradle 'init' task.
 *
 * This generated file contains a sample Java project to get you started.
 * For more details take a look at the Java Quickstart chapter in the Gradle
 * User Manual available at https://docs.gradle.org/6.3/userguide/tutorial_java_projects.html
 */

plugins {
    // Apply the java plugin to add support for Java
    java

    // Apply the application plugin to add support for building a CLI application.
    application

    checkstyle
}

repositories {
    // Use jcenter for resolving dependencies.
    // You can declare any Maven/Ivy/file repository here.
    jcenter()

    // third-party (non-distributable, probably due to licenses) artifacts
    flatDir {
        dirs("third-party")
    }
}

dependencies {
    // This dependency is used by the application.
    implementation(":ConsolePG")

    // Use JUnit Jupiter API for testing.
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")

    // Use JUnit Jupiter Engine for testing.
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")
}

application {
    // Define the main class for the application.
    mainClassName = "pt.pedrodavid.tetris.Tetris"
}

checkstyle {
    toolVersion = "8.31"
    // From: https://raw.githubusercontent.com/checkstyle/checkstyle/checkstyle-8.31/src/main/resources/google_checks.xml
    configFile = file("config/checkstyle/checkstyle-8.31-google_checks.xml")
//    maxWarnings = 0
//    config = project("com.puppycrawl.tools:checkstyle:8.29").resources.text.fromFile("google_checks.xml")
}

val test by tasks.getting(Test::class) {
    // Use junit platform for unit tests
    useJUnitPlatform()
}
