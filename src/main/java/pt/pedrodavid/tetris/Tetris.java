package pt.pedrodavid.tetris;

import java.awt.event.KeyEvent;

import isel.leic.pg.Console;

import static isel.leic.pg.Console.NO_KEY;

public class Tetris {

    private static final int STEP_TIME = 1000; // 1 second by each step
    /**
     * The game state
     **/
    private static long nextStepTime; // Time to call next step()
    private static Piece piece;          // Current piece controlled
    private static Piece nextpiece;
    private static boolean gameOver = false, close = false;
    public static boolean sound = true;

    public static void main(String[] args) {
        init();
        do {
            mute();
            mute();
            Score.drawHUD();
            piece = new Piece();    // The first piece
            nextpiece = new Piece();
            piece.show();
            nextpiece.showNext();
            nextStepTime = System.currentTimeMillis() + STEP_TIME;
            run();                    // Run the game
            terminate();
        } while (!close);
        Console.close();
    }

    private static final int
            LINES = Board.DIM_LINES + Board.BASE_LINE, COLS = Board.DIM_COLS + (Board.BASE_COL * 2);

    private static final String GAME_OVER_TXT = "GAME  OVER",
            PLAY_AGAIN_TXT = "RETRY? Y/N" /*, GAME_PAUSED_TXT = "PAUSED"*/;

    private static void init() {
        Console.open("PG Tetris", LINES + 1, COLS + 22);
        Console.exit(true);                    // Enable exit console
    }

    private static void terminate() {
        gameOver = false;
//		Console.stopMusic();
        Console.cursor(LINES / 2 - 1, (COLS - GAME_OVER_TXT.length()) / 2);
        Console.color(Console.RED, Console.YELLOW);
        Console.print(GAME_OVER_TXT);        // Message GAME OVER
        Console.cursor(LINES / 2 + 1, (COLS - PLAY_AGAIN_TXT.length()) / 2);
        Console.print(PLAY_AGAIN_TXT);
        askExit();
    }

    private static void run() {
        int key = NO_KEY;
        long waitTime;                        // Time to wait for next step
        do {
            waitTime = nextStepTime - System.currentTimeMillis();
            if (waitTime <= 0)
                step();                        // Next step of the game
            else {
                key = Console.waitKeyPressed(waitTime);
                if (key != NO_KEY) {        // A key was pressed ?
                    action(key);            // Do action for the key
                    while (Console.isKeyPressed(key))  // Wait to release key
                        if (System.currentTimeMillis() >= nextStepTime)
                            step();            // Next step with the key held down
                }
            }
        } while (!gameOver);
    }

    private static void step() {
        if (!piece.down()) {            // If possible, move the piece down
            piece.hide();                // Hide the piece that hit bottom
            piece.putTrue();
            Board.checkLines();
            Score.drawScore();
            Board.drawGameBoard();
            if (nextpiece.possible() == true) {
                piece = nextpiece;        // Next piece becomes current piece
                piece.show();            // Show the new piece
                nextpiece.hideNext();
                nextpiece = new Piece();// Create a new next piece
                nextpiece.showNext();
            } else gameOver = true;
        }
        nextStepTime += STEP_TIME - Score.currentSpeed();  // Set next time to move the piece
    }

    private static void action(int key) {
        switch (key) {
            case KeyEvent.VK_LEFT:
                piece.moveLeft();
//			if(sound)
//				Console.playSound("move");
                break;
            case KeyEvent.VK_RIGHT:
                piece.moveRight();
//			if(sound)
//				Console.playSound("move");
                break;
            case KeyEvent.VK_Q:
                piece.rotateLeft();
                break;
            case KeyEvent.VK_W:
                piece.rotateRight();
                break;
            case KeyEvent.VK_DOWN:
                piece.down();
//			if(sound)
//				Console.playSound("move");
                break;
            case KeyEvent.VK_ESCAPE:
                gameOver = true;
                break;
            case KeyEvent.VK_SPACE:
                piece.instaDown();
                break;
            case KeyEvent.VK_M:
                mute();
                break;
        }
    }

    public static void askExit() {
        int key = NO_KEY;
        key = Console.waitKeyPressed(0);
        if (key == KeyEvent.VK_Y || key == KeyEvent.VK_ENTER)
            Score.resetgame();        // Reset the game
        else if (key == KeyEvent.VK_N || key == KeyEvent.VK_ESCAPE)
            close = true;                // Close Console window
        else
            askExit();
    }

    private static void mute() {
        Console.cursor(LINES, COLS + 17);
        if (sound) {
            sound = false;
//			Console.stopMusic();
            Console.color(Console.GRAY, Console.BLACK);
        } else {
            sound = true;
//			Console.startMusic("MainTheme");
            Console.color(Console.YELLOW, Console.BLACK);
        }
        Console.print("MUSIC");
    }
}
