package pt.pedrodavid.tetris;

import isel.leic.pg.Console;

public class Piece {
    private final int type;    // Type of piece (I,J,L,O,S,T or Z)
    private int line, column;    // Current position
    private int direction;        // Current direction (0,1,2 or 3)

    public Piece(int type) {    // Create a piece of one type
        this.type = type;
        column = Board.DIM_COLS / 2 - GRID_COLS / 2;  // Centered in board
        line = 0;                         // Top line
        direction = 0;                 // First direction
    }

    public Piece() {    // Piece of random type
        this((int) (Math.random() * (Piece.Z + 1)));
    }

    private static final int MAX_DIRECTIONS = 4;

    public boolean rotateLeft() {
        return rotate(MAX_DIRECTIONS - 1);
    }

    public boolean rotateRight() {
        return rotate(1);
    }

    private boolean rotate(int by) {
        int dir = (direction + by) % MAX_DIRECTIONS;        // The new direction
        if (!validBlocks(line, column, BLOCKS[type][dir]))  // Is possible ?
            return false;
        hide();
        direction = dir;
        show();
//      if(Tetris.sound)
//      	Console.playSound("rotate");
        return true;
    }

    public boolean moveLeft() {
        return move(0, -1);
    }

    public boolean moveRight() {
        return move(0, +1);
    }

    public boolean down() {
        return move(1, 0);
    }

    public boolean possible() {
        return move(0, 0);
    }

    public boolean instaDown() {
        return moveDownMax();
    }


    private boolean moveDownMax() {    //Move Down as much as possible
        int addscore = 0;
        for (int i = 0; i < Board.DIM_LINES; ++i)
            if (validBlocks(line + 1, column, BLOCKS[type][direction])) {
                hide();
                line++;
                show();
                addscore++;
            }
        Score.dropscore(addscore); //Award points for how many skipped lines
//    	if(Tetris.sound)
//    		Console.playSound("drop");
        return true;
    }

    private boolean move(int dLine, int dCol) {
        if (!validBlocks(line + dLine, column + dCol, BLOCKS[type][direction]))
            return false;        // If the new position is not possible
        hide();
        line += dLine;
        column += dCol;
        show();
        return true;
    }

    private static boolean validBlocks(int line, int col, int blocks) {
        int mask = MASK_INIT;
        for (int l = 0; l < GRID_LINES; ++l)                // For each line
            for (int c = 0; c < GRID_COLS; ++c, mask >>= 1)    // For each column
                if ((blocks & mask) != 0 && !Board.validPosition(line + l, col + c))
                    return false;                // If the block has no room
        return true;
    }

    private static void draw(int line, int col, int color, int blocks, char txt) {
        Console.color(Console.WHITE, color);
        int mask = MASK_INIT;
        for (int l = 0; l < GRID_LINES; ++l)                // For each line
            for (int c = 0; c < GRID_COLS; ++c, mask >>= 1)    // For each column
                if ((blocks & mask) != 0) {        // If has a block
                    Console.cursor(line + l, col + c); // Mover cursor
                    Console.print(txt);            // Write the char for the block
                }
    }

    public static final int  // Type of Tetriminos 
            I = 0, J = 1, L = 2, O = 3, S = 4, T = 5, Z = 6;

    public static final int[] COLORS = {  // Colors of Tetriminos
            Console.RED    /*I*/, Console.BLUE   /*J*/, Console.ORANGE/*L*/,
            Console.YELLOW /*O*/, Console.MAGENTA/*S*/, Console.CYAN  /*T*/,
            Console.GREEN  /*Z*/
    };

    private static final int GRID_LINES = 4, GRID_COLS = 4;
    private static final int MASK_INIT = 0x8000;

    public static final int[][] BLOCKS = { // Blocks of Tetriminos in 4 directions
            {0x0F00, 0x4444, 0x0F00, 0x2222},  //I
            {0x2260, 0x8E00, 0x6440, 0x0710},  //J
            {0x4460, 0x0E80, 0x6220, 0x0170},  //L
            {0x6600, 0x6600, 0x6600, 0x6600},  //O
            {0x4620, 0x06C0, 0x4620, 0x0360},  //S
            {0x2620, 0x04E0, 0x4640, 0x0720},  //T
            {0x2640, 0x0C60, 0x2640, 0x0630},  //Z
    };

    /* SHOW PIECES */

    public void show() {  // Show the blocks of the piece in board
        draw(Board.BASE_LINE + line, Board.BASE_COL + column,
                COLORS[type], BLOCKS[type][direction], 'o');
    }

    public void showNext() {  // Show the blocks of the next piece
        draw(9, Board.BASE_COL * 2 + 1 + Board.DIM_COLS,
                COLORS[type], BLOCKS[type][direction], 'o');
    }

    public void putTrue() {  // Put the blocks of the piece in true board
        turnToTrue(Board.BASE_LINE + line, Board.BASE_COL + column,
                COLORS[type], BLOCKS[type][direction]);
    }

    public static void showEach(int line, int col, int type, int position) {  // Show the blocks of each piece
        draw(line, col, COLORS[type], BLOCKS[type][position], 'o');
    }

    /* HIDE PIECES */

    public void hide() {  // Show the grid in the previous position of the blocks
        draw(Board.BASE_LINE + line, Board.BASE_COL + column,
                Console.BLACK, BLOCKS[type][direction], '.');
    }

    public void hideNext() {  // Hide the blocks of the "previous next piece"
        draw(9, Board.BASE_COL * 2 + 1 + Board.DIM_COLS,
                Console.BLACK, BLOCKS[type][direction], ' ');
    }

    /* PRINT PIECES IN THE ARRAY */

    private static void turnToTrue(int line, int col, int color, int blocks) {
        int mask = MASK_INIT;
        for (int l = 0; l < GRID_LINES; ++l)
            for (int c = 0; c < GRID_COLS; ++c, mask >>= 1)
                if ((blocks & mask) != 0)
                    Board.printBlocks(line + l, col + c, color);
        Score.addPiece(color - 2);            //Add the piece to stats
        Score.addPieceScore(2);
    }
}
