package pt.pedrodavid.tetris;

import isel.leic.pg.Console;

public class Board {

    public static final int
            BASE_LINE = 0, BASE_COL = 1,
            DIM_LINES = 20, DIM_COLS = 10;

    private static int[][] trueboard = new int[DIM_LINES][DIM_COLS];        // GAME BOARD

    public static void clearBoard() {
        trueboard = new int[Board.DIM_LINES][Board.DIM_COLS];
    }    //clear gameboard after restart

    public static void drawGrid() {                    //Draw game field
        Console.color(Console.WHITE, Console.BLACK);
        for (int l = 0; l < DIM_LINES; ++l)
            for (int c = 0; c < DIM_COLS; ++c)
                put(BASE_LINE + l, BASE_COL + c, '.');
    }

    public static void drawBorder() {                //Draw game field borders
        Console.color(Console.BLACK, Console.WHITE);
        for (int l = 0; l <= DIM_LINES + BASE_LINE; ++l)
            for (int c = BASE_COL - 1; c <= DIM_COLS + BASE_COL; ++c) {
                if (l == BASE_LINE - 1 || l == DIM_LINES + BASE_LINE) {
                    if ((c == BASE_COL - 1 || c == DIM_COLS + BASE_COL) && BASE_COL > 0)
                        put(l, c, '+');
                    else if (c > BASE_COL - 1 && c < DIM_COLS + BASE_COL)
                        put(l, c, '-');
                } else if (l >= BASE_LINE && (c == BASE_COL - 1 || c == DIM_COLS + BASE_COL) && BASE_COL > 0)
                    put(l, c, '|');
            }
    }

    public static void put(int line, int col, char c) {
        Console.cursor(line, col);
        Console.print(c);
    }

    public static boolean validPosition(int line, int col) {
        return line >= 0 && line < DIM_LINES && col >= 0 && col < DIM_COLS && trueboard[line][col] == 0;
    }

    public static void printBlocks(int line, int col, int type) {
        trueboard[line - BASE_LINE][col - BASE_COL] = type;
    }

    public static void drawGameBoard() {            //Print the GAME BOARD
        for (int l = 0; l < DIM_LINES; ++l)
            for (int c = 0; c < DIM_COLS; ++c) {
                if (trueboard[l][c] == 0) {
                    Console.color(Console.WHITE, Console.BLACK);
                    put(BASE_LINE + l, BASE_COL + c, '.');
                } else {
                    Console.color(Console.WHITE, trueboard[l][c]);
                    put(BASE_LINE + l, BASE_COL + c, ' ');
                }
            }
    }

    public static void checkLines() {                    //Check each Line
        int completedlines = 0;
        int[] cleartopline = new int[DIM_COLS];
        for (int l = 0; l < DIM_LINES; ++l)
            if (checkLine(l)) {
                completedlines++;
                trueboard[0] = cleartopline;            //clear top line when  a line is cleared
            }
        Score.importlines(completedlines);            //export the number of lines completed to score
    }

    private static boolean checkLine(int l) {        //check if the Line is completed
        for (int c = 0; c < DIM_COLS; ++c)
            if (trueboard[l][c] == 0)
                return false;

        for (int row = l - 1; row >= 0; --row)
            for (int c = 0; c < DIM_COLS; ++c)
                trueboard[row + 1][c] = trueboard[row][c];    //cleared line=line on top(...)
        return true;
    }
}
