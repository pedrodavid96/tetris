package pt.pedrodavid.tetris;

import isel.leic.pg.Console;

public class Score {

    private static final int linesPerLevel = 10, speedPerLevel = 50;

    private static int totalLines = 0, score = 0, level = 0;
    private static int[] eachPiece = new int[7];            //Amount of each piece put into the field
    private static int[] comboAmount = new int[4];        //Amount of combos (1x/2x/3x/4x)

    private static final String SCORE_TXT = "SCORE:", LINES_TXT = "LINES:",
            LEVEL_TXT = "LEVEL:", NEXT_TXT = "NEXT";

    private static final String[] Instructions = {
            " Left - Move left   ", "Right - Move right  ", "    Q - Rotate left ",
            "    W - Rotate right", " Down - Move Down   ", "Space - Drop        ",
            "  ESC - Terminate   "};

    public static int currentSpeed() {
        return speedPerLevel * level;
    }

    public static void importlines(int clearedlines) {        //Receives how much lines where cleared in that round
        clearscore(clearedlines);
        changelevel();
    }

    /*Plays sound accordingly to the combo, adds combo amount and respective score and add lines*/

    private static void clearscore(int clearedlines) {
        switch (clearedlines) {
            case 0:
//			if(Tetris.sound)
//				Console.playSound("pop");
                break;
            case 1:
                score += (40 * (1 + level));
//			if(Tetris.sound)
//				Console.playSound("line");
                ++comboAmount[0];
                break;
            case 2:
                score += (100 * (1 + level));
//			if(Tetris.sound)
//				Console.playSound("double");
                ++comboAmount[1];
                break;
            case 3:
                score += (300 * (1 + level));
//			if(Tetris.sound)
//				Console.playSound("tripple");
                ++comboAmount[2];
                break;
            case 4:
                score += (1200 * (1 + level));
//			if(Tetris.sound)
//				Console.playSound("tetris");
                ++comboAmount[3];
                break;
        }
        totalLines += clearedlines;
    }

    /* ***** ADD SCORE ***** */
    public static void dropscore(int addscore) {
        score += (addscore / 2 * (level + 1));
    }

    public static void addPieceScore(int addscore) {
        score += (addscore * (level + 1));
    }

    private static int changelevel() {
        return level = totalLines / linesPerLevel;
    }

    public static void addPiece(int piece) {
        eachPiece[piece]++;
    }

    private static void putInt(int line, int col, int c) {
        Console.cursor(line, col);
        Console.print(c);
    }

    /* ***** DRAW HUD ***** */

    public static void drawInstructions() {
        Console.color(Console.WHITE, Console.GRAY);
        int length = Instructions.length;
        for (int i = 0; i < length; ++i) {
            Console.cursor(Board.DIM_LINES - 1 - i, 2 * Board.BASE_COL + Board.DIM_COLS + 1);
            Console.println(Instructions[length - 1 - i]);
        }
    }

    private static void showEachPiece() {
        Piece.showEach(4, Board.BASE_COL * 2 + Board.DIM_COLS + 8, 0, 0);
        Piece.showEach(4, Board.BASE_COL * 2 + Board.DIM_COLS + 12, 3, 0);
        Piece.showEach(3, Board.BASE_COL * 2 + Board.DIM_COLS + 16, 5, 3);
        Piece.showEach(7, Board.BASE_COL * 2 + Board.DIM_COLS + 8, 1, 1);
        Piece.showEach(6, Board.BASE_COL * 2 + Board.DIM_COLS + 12, 4, 3);
        Piece.showEach(9, Board.BASE_COL * 2 + Board.DIM_COLS + 8, 2, 1);
        Piece.showEach(9, Board.BASE_COL * 2 + Board.DIM_COLS + 12, 6, 3);
    }

    private static void numberEachPiece() {
        Console.color(Console.WHITE, Console.GRAY);
        Console.cursor(4, Board.BASE_COL * 2 + Board.DIM_COLS + 9);
        Console.print(eachPiece[0]);
        Console.cursor(5, Board.BASE_COL * 2 + Board.DIM_COLS + 15);
        Console.print(eachPiece[3]);
        Console.cursor(5, Board.BASE_COL * 2 + Board.DIM_COLS + 19);
        Console.print(eachPiece[6]);
        Console.cursor(7, Board.BASE_COL * 2 + Board.DIM_COLS + 10);
        Console.print(eachPiece[2]);
        Console.cursor(8, Board.BASE_COL * 2 + Board.DIM_COLS + 15);
        Console.print(eachPiece[4]);
        Console.cursor(11, Board.BASE_COL * 2 + Board.DIM_COLS + 10);
        Console.print(eachPiece[5]);
        Console.cursor(10, Board.BASE_COL * 2 + Board.DIM_COLS + 15);
        Console.print(eachPiece[1]);
    }

    private static void numberEachCombo() {
        Console.color(Console.WHITE, Console.GRAY);
        for (int i = 0; i < 4; ++i)
            putInt(2 + i, Board.BASE_COL * 2 + Board.DIM_COLS + 4, comboAmount[i]);
    }

    private static void drawText() {
        Console.color(Console.YELLOW, Console.BLACK);
        Console.cursor(1, Board.BASE_COL * 2 + Board.DIM_COLS + 13);
        Console.print(LEVEL_TXT);
        Console.color(Console.WHITE, Console.BLACK);
        Console.cursor(1, Board.BASE_COL * 2 + Board.DIM_COLS + 1);
        Console.print(SCORE_TXT);
        Console.cursor(2, Board.BASE_COL * 2 + Board.DIM_COLS + 12);
        Console.print(LINES_TXT);
        Console.cursor(7, Board.BASE_COL * 2 + Board.DIM_COLS + 1);
        Console.print(NEXT_TXT);
        for (int i = 0; i < 4; ++i) {
            Console.cursor(2 + i, Board.BASE_COL * 2 + Board.DIM_COLS + 1);
            Console.print("T" + (i + 1) + ":");
        }
    }

    public static void drawScore() {
        Console.color(Console.YELLOW, Console.BLACK);
        putInt(1, Board.BASE_COL * 2 + Board.DIM_COLS + 19, level);
        Console.color(Console.WHITE, Console.GRAY);
        putInt(1, Board.BASE_COL * 2 + Board.DIM_COLS + 7, score);
        putInt(2, Board.BASE_COL * 2 + Board.DIM_COLS + 18, totalLines);
        numberEachPiece();
        numberEachCombo();
    }

    public static void drawHUD() {
        Board.drawGrid();                    // Draw initial board
        Board.drawBorder();                    // Draw the Borders of the board
        drawInstructions();
        drawText();
        showEachPiece();
        drawScore();
    }

    /* ***** RESET GAME HUD ***** */

    public static void resetgame() {
        Console.color(Console.WHITE, Console.BLACK);
        Console.clear();
        totalLines = 0;
        score = 0;
        level = 0;
        comboAmount = new int[4];
        eachPiece = new int[7];
        Board.clearBoard();
    }
}
